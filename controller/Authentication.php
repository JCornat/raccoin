<?php


namespace controller;


use \exception\AuthException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\Membre;

class Authentication {

    public static function authenticate($email, $password) {
        try {
            $membre = Membre::where("email", "=", $email)->firstOrFail();
            if (!password_verify($password, $membre->mot_passe)) {
                throw new AuthException();
            }

        } catch(ModelNotFoundException $e) {
            throw new AuthException();
        }
    }

    public static function loadProfile($email) {
        try {
            $membre = Membre::where("email", "=", $email)->firstOrFail();
            $_SESSION['connecte'] = true;
            $_SESSION['id'] = $membre->id;
            $_SESSION['nom'] = $membre->nom;
            $_SESSION['prenom'] = $membre->prenom;
            $_SESSION['email'] = $membre->email;
            $_SESSION['telephone'] = $membre->telephone;
            $_SESSION['code_postal'] = $membre->code_postal;
        } catch(ModelNotFoundException $e) {
            throw new AuthException();
        }
    }

    public static function checkAccessRights($required) {

    }
}