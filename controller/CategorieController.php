<?php
namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\Annonce;
use model\Categorie;
use view\ListeAnnonces;

class CategorieController extends BaseController {

    public function annonces($id){

        try {
            $categorie = Categorie::where("id", "=", $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $this->app->error("Catégorie non disponible");
        }
        $annonces = Annonce::where('id_categorie', '=', $id)->orderBy('date_creation', 'desc')->get();

        if(sizeof($annonces) == 0) {
            $this->app->error("Pas d'annonces dans cette catégorie");
        }
        foreach($annonces as $annonce) {
            $annonce['url'] = $this->app->urlFor('annonce', array('id' => $annonce['id']) );
            $annonce['image'] = $annonce->cover();
        }

        $view = new ListeAnnonces();
        $view->addVar('link',links());
        $view->addVar('annonces', $annonces);
        $view->addVar('titre', "Annonces pour la catégorie ".$categorie->nom);
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        $view->addVar('session', $_SESSION);
        echo $view->render();
    }

}