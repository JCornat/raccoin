CREATE DATABASE  IF NOT EXISTS `Racoin` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `Racoin`;
-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: Racoin
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) DEFAULT NULL,
  `descriptif` varchar(1024) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `id_membre` int(11) DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_annonce_1_idx` (`id_membre`),
  KEY `fk_annonce_2_idx` (`id_categorie`),
  CONSTRAINT `fk_annonce_1` FOREIGN KEY (`id_membre`) REFERENCES `membre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_annonce_2` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annonce`
--

LOCK TABLES `annonce` WRITE;
/*!40000 ALTER TABLE `annonce` DISABLE KEYS */;
INSERT INTO `annonce` VALUES (1,'Nissan GT-R','Puissante, racée, sculpturale, bestiale, la GT-R de Nissan mise sur le marché en 2007 et relookée déjà trois fois, en 2011, en 2012 et en 2013, est un véritable bolide de compétition qui fait frémir la 911 de Porsche ! ',150000,1,'2015-02-07 13:17:39',1,NULL),(2,'Lamborghini Aventador','La Lamborghini Aventador LP700-4, connue en interne sous les codes « LB834 » (coupé) et « LB835 » (roadster), est une supercar développée par le constructeur italien Lamborghini. Dévoilée au Salon de Genève 2011, elle remplace la Lamborghini Murciélago.',519000,2,'2015-02-07 13:20:51',1,NULL),(3,'CSS2','Norme CSS un peu viellote à vendre. Mauvais état.',10,1,'2015-02-07 13:22:24',3,NULL);
/*!40000 ALTER TABLE `annonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'Véhicule','http://puu.sh/elRg7/bd32ee8133.jpg'),(2,'Immobilier','http://puu.sh/enktl/77d750e634.jpg'),(3,'Multimédia','http://i.computer-bild.de/imgs/1/6/7/7/1/6/6/Acer-Aspire-Predator-500x500-e3831f489134455d.jpg'),(4,'Vêtements','http://puu.sh/enkMz/ec669eaa33.jpg'),(5,'Montre','http://www.montrezine.com/wp-content/uploads/2014/01/dior-montre-femme.jpg'),(6,'Bijoux','http://www.aboveluxe.fr/new/wp-content/uploads/2009/09/bijoux-bea-valdes-2.png'),(7,'Parfum','http://www.vogue.fr/uploads/images/thumbs/201129/yves_saint_lurent_8072_north_430x.jpg');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_annonce` int(11) DEFAULT NULL,
  `position` int(1) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_1_idx` (`id_annonce`),
  CONSTRAINT `fk_images_1` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,1,1,'photos/annonce-1/photo1.jpg'),(2,1,2,'photos/annonce-1/photo2.jpg'),(3,2,1,'photos/annonce-2/photo1.jpg'),(4,2,2,'photos/annonce-2/photo2.jpg'),(5,3,1,'photos/annonce-3/photo1.png');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre`
--

DROP TABLE IF EXISTS `membre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `mot_passe` varchar(256) DEFAULT NULL,
  `enregistre` varchar(1) DEFAULT NULL,
  `code_postal` varchar(5) DEFAULT NULL,
  `api_key` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre`
--

LOCK TABLES `membre` WRITE;
/*!40000 ALTER TABLE `membre` DISABLE KEYS */;
INSERT INTO `membre` VALUES (1,'Cornat','Jacques','test@test.com','0707070707','$2y$12$RRXB9qUDnrpgARM7p8Feo.vqt/0h9iOflW6dZa72FZSH7vtsjmHSe',NULL,'54000',NULL),(2,'Michel','Julien','test@test2.com','0808080808','$2y$12$oGM9YFBoWpVhRedxS8k9weqcZwk0QwpnvszgxErHTYxtXZMyRAee2',NULL,'54000',NULL);
/*!40000 ALTER TABLE `membre` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-07 13:24:42
