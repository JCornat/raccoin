<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model{

    public $table = 'categorie';
    public $idTable = 'id';
    public $timestamps = false;

}